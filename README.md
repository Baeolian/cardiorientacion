# Proyecto Cardiorientación

Nuestra meta es, dado unos datos de sensores de orientación, determinar la posición de un paciente *en tiempo real*. En concreto, si está sentado/de pie o tumbado.

## Archivos de entrada

Se tratan de *datasets* en formato **CSV**  generados directamente desde un dispositivo móvil y parseables mediante filas y *comas* ( **,** ).

### Accelerometer, Compass & Gyroscope

|Timestamp|Millisecond|X|Y|Z|
|--|--|--|--|--|
|2020-03-25 12:39:42|0|-0.0191541|0.0095770|9.783007
|2020-03-25 12:39:42|101|0.0|0.0119713|9.797373

## Estudio

- Dispositivo principal, **BNO055** ([datasheet](http://handlebarsjs.com/)), incluye:
	- Acelerómetro de 14 bits
	- Sensor geomagnético
	- Giroscopio de 16 bits

> **Nota 1**: el más interesante y útil de todos a la hora de determinar la posición de un paciente es el **Acelerómetro**.

> **Nota 2**: al estar todos los sensores en el mismo chip, determinamos los ejes de trabajo de todos los sensores según la siguiente imagen:

![](https://i.ibb.co/YZLJVPv/coords.png)

- Dispositivos adicionales (modelos desconocidos):
	- Sensor de luz
	- Sensor de presión

## ⁉ Dudas sin resolver (Resueltas 3/3)

- ~~Hay periodos de segundos en los que los datos de entrada para el sensor BNO055 son los mismos y no cambian. ¿A qué se debe? Porque la precisión es muy grande y se puede observar cómo *tiembla* el dispositivo continuamente.~~
> ~~Creemos que es debido a que el dispositivo pierde de alguna forma la comunicación con el sensor.~~

✅ Hay muestras redundantes a lo largo de algunos datasets que hay que filtrar. Se puede comprobar fijándonos en el campo *Milliseconds* (las muestras son adquiridas cada 100ms).

- ~~¿Dónde y cómo tiene colocado el dispositivo el paciente?~~
	- ~~¿En el pecho?~~
	- ~~¿En la mano? ¿Dónde se pondría, entonces, la mano?~~
> ~~Esta es una pregunta crucial, ya que no podemos suponer nada. Hay que buscarle el sentido más a fondo a los datos del sensor.~~

✅ El paciente tiene el teléfono móvil colocado en el **tobillo** de la siguiente forma:

![](https://o.quizlet.com/yPWNr9vnLk1Ksu4.8LAXZQ.jpg)

- ~~¿Cómo sabemos cuando el paciente está de pie o sentado? Si el dispositivo está en el tobillo, no va a haber diferencia cuando esté sentado o de pie. Ningún dato de giroscopio, acelerómetro, brújula, etc. va a tener cambios significativos en ninguno de sus ejes.~~

✅  Exactamente, no existe diferencia entre estar sentado o de pie si el dispositivo está en el tobillo.

## Trabajo

### 🔅 VI principal: Accelerometer.vi

- [x] Representación gráfica de los ejes
- [x] Tiempo Real
- [x] Representación objeto 3D
- [x] Testeo

### 🔅 SubVI's

#### Gyroscope.vi

- [x] Representación gráfica de los ejes
- [x] Tiempo Real
- [x] Representación objeto 3D

#### Pressure.vi

- [x] Representación gráfica
- [x] Tiempo Real

#### Compass.vi

- [x] Representación gráfica de los ejes
- [x] Tiempo Real
- [x] Representación objeto 3D

#### Light.vi

- [x] Representación gráfica
- [x] Tiempo Real


