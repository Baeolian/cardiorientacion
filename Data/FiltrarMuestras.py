import csv
import os
import glob

if __name__ == '__main__':

    path = '../Data/Originales'
    extension = 'csv'
    os.chdir(path)
    result = glob.glob('*.{}'.format(extension))
    print(result)

    rowBefore = None

    for file in result:
        with open(file) as csv_original:
            csv_reader = csv.reader(csv_original, delimiter=',')
            line_count = 0
            file = file.replace('.csv', 'Filtered.csv')
            with open(file, mode='w', newline='') as csv2:
                writer = csv.writer(csv2)
                for row in csv_reader:
                    if row != rowBefore:
                        writer.writerow(row)
                    rowBefore = row
